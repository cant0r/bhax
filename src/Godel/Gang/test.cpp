#include <iostream>
#include <vector>
#include <algorithm>


int main()
{
    std::vector<int> nums = {3, 1, 0, -1, -4,};
    
    std::cout << "Before sorting:\n";
    for(auto& i : nums)
        std::cout << i << " ";
    std::cout << std::endl;
    
    std::sort(nums.begin(), nums.end(), 
        [](int a, int b){ return a < b;}
    );
    
    std::cout << "After sorting:\n";
    for(auto& i : nums)
        std::cout << i << " ";
    std::cout << std::endl;
    
}
