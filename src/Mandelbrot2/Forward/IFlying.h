#include <exception>
using namespace std;

#ifndef __IFlying_h__
#define __IFlying_h__

#include "Bird.h"

// class Bird;
class IFlying;

__abstract class IFlying: public Bird
{

	public: virtual bool fly() = 0;

	public: virtual bool walkAround() = 0;

	public: virtual bool land() = 0;
};

#endif
