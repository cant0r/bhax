#include <exception>
using namespace std;

#ifndef __Raven_h__
#define __Raven_h__

#include "IFlying.h"

// class IFlying;
class Raven;

class Raven: public IFlying
{
	private: string _flying;

	public: bool fly();

	public: bool walkAround();

	public: bool land();
};

#endif
