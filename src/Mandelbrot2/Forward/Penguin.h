#include <exception>
using namespace std;

#ifndef __Penguin_h__
#define __Penguin_h__

#include "INotFlying.h"

// class INotFlying;
class Penguin;

__abstract class Penguin: public INotFlying
{
	private: string _jumping;

	public: bool walkAround();

	public: bool jumpAround();
};

#endif
