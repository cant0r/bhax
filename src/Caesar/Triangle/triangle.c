#include <stdio.h>
#include <stdlib.h>

#define SIZE 10


int main()
{
    double** ultimateMatrix;
    printf("A double**-ra mutató címe: %p\n", &ultimateMatrix);    

    ultimateMatrix = (double**) malloc(SIZE* sizeof(double*));

    printf("A double** első sorának első elemének címe: %p\n", ultimateMatrix);    

    if(ultimateMatrix)
    {
        for(int i=0; i<SIZE; i++)
        {
            if(!(ultimateMatrix[i] = (double*) malloc((i+1)*sizeof(double))))
            {
                printf("MEMÓRIAFOGLALÁSI HIBA!!\n");
                return -1;
            }
        }
        
        printf("A double** első elemének(1. sor 1. oszlopa) címe: %p\n", ultimateMatrix[0]);    
        
        //Filling it up
        for(int i=0; i<SIZE; i++)
        {
            for(int j=0; j<i+1; j++)
            {
                ultimateMatrix[i][j] = (i*(i+1))/2 +j; //see triangle matrices for expl
            }
        }

        //Testing ground for pointer aritm
        ultimateMatrix[3][0] = 42.0;
        (*(ultimateMatrix +3))[1] = 43.0;
        *(ultimateMatrix[3] + 2) = 44.0;
        *(*(ultimateMatrix + 3) + 3) = 45.0;
        
        
        for(int i=0; i<SIZE; i++)
            for(int j=0; j<i+1; j++)
            {
                printf("%4.1f", ultimateMatrix[i][j]);
                
                if(j==i)
                    putchar('\n');
                else
                    putchar(' ');
            }
            
        
          for(int i=0; i<SIZE; i++)
                  free(ultimateMatrix[i]);
          free(ultimateMatrix);
        
        
        
    }
    else
    {
        printf("MEMÓRIAFOGLALÁSI HIBA!!\n");
        return -1;
    }
    
    return 0;
}
