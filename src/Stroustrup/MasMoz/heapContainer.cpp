#include <iostream>

#define DEFAULT 16

class HeapContainer
{
    int* buffer = nullptr;
    unsigned int numOfItems = DEFAULT;
public:    
    HeapContainer()
    {
        buffer = new int[DEFAULT];
    }
    HeapContainer(const HeapContainer& other)
    {
        if(this != &other)
        {
            *this = other;
        }
    }
    HeapContainer& operator=(const HeapContainer& other)
    {
        numOfItems = other.numOfItems;
        delete[] buffer;
        buffer = new int[numOfItems];
        
        for(int i=0; i<numOfItems; i++)
        {
            buffer[i] = other.buffer[i];
        }
        return *this;
    }
    HeapContainer(HeapContainer&& other)
    {
        if(buffer)
            delete[] buffer;
        buffer = nullptr;
        *this = std::move(other);
    }
    HeapContainer& operator=(HeapContainer&& other)
    {
        std::swap(buffer,other.buffer);
        numOfItems = other.numOfItems;
        return *this;
    }
    ~HeapContainer()
    {
        delete[] buffer;
    }
    
    void put(int pos, int n)
    {
        if(pos >= 0 && pos < numOfItems)
        {
            buffer[pos] = n;
        }
    }
    int get(int pos)
    {
        if(pos >= 0 && pos < numOfItems)
        {
            return buffer[pos];
        }
        return -666;
    }
    friend std::ostream& operator<<(std::ostream& out, HeapContainer& h)
    {
        for(int i=0; i<h.numOfItems; i++)
            out << h.buffer[i] << " ";
        out << "\n";
        return out;
    }
};

int main()
{
    HeapContainer theContainer;
    
    theContainer.put(0,5);
    theContainer.put(1,5);
    
    std::cout << "Eredeti konténer:\n";
    std::cout << theContainer;
    
    HeapContainer anotherOne = theContainer;
    std::cout << "Másolt konténer:\n";
    std::cout << anotherOne;
    
    
    HeapContainer movedOne = std::move(anotherOne);
    std::cout << "Mozgatott konténer:\n";
    std::cout << movedOne;
    return 0;
}
