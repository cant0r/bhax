import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Random;


public class CUSTOMRSA {
     static class KeyPair
    {
        BigInteger d,e,m;
        public KeyPair()
        {
             int inBites = 700 * (int) (Math.log((double) 10)/
                Math.log((double) 2));


            BigInteger p=
                new BigInteger(inBites, 100, new Random());

            BigInteger q=
                new BigInteger(inBites, 100, new Random());

            m = p.multiply(q);


            BigInteger z = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));


            do
            {
                do
                {
                    d = new BigInteger(inBites, new Random());
                } while(d.equals(BigInteger.ONE));
            }while(!z.gcd(d).equals(BigInteger.ONE));



           e = d.modInverse(z);
        }
    
    }
    
    static class Pair 
    {

        private String encrypted;
        private char letter = ' ';
        private int freq = 0;

        public Pair(String str, char k) {
            encrypted = str;
            letter = k;
        }

        public Pair(String str) {
            encrypted = str;
        }

        public void setEncrypted(String str) {
            encrypted = str;
        }

        public String getEncrypted() {
            return encrypted;
        }

        public void setLetter(char k) {
            letter = k;
        }

        public char getLetter() {
            return letter;
        }

        public void increment() {
            freq += 1;
        }

        public int getFreq() {
            return freq;
        }
}
    
    public static void main(String[] args) throws FileNotFoundException,IOException 
    {

        KeyPair theKey = new KeyPair();

        String cleanText = "Free software developers guarantee everyone equal rights to their programs; any user can study the source code, modify it, and share the program. By contrast, most software carries fine print that denies users these basic rights, leaving them susceptible to the whims of its owners and vulnerable to surveillance.";

        System.out.println("Before applying faulty RSA:");
        System.out.println(cleanText);
        
        PrintWriter writer = new PrintWriter("output.txt");
        for (int idx = 0; idx < cleanText.length(); ++idx) 
        {
            byte[] buffer = cleanText.substring(idx,idx+1).toLowerCase().getBytes();
            java.math.BigInteger[] secret = new java.math.BigInteger[buffer.length];
           
            for (int i = 0; i < secret.length; ++i) 
            {
                secret[i] = new java.math.BigInteger(new byte[]{buffer[i]});
                secret[i] = secret[i].modPow(theKey.e, theKey.m);
                writer.print(secret[i]);
            }
            writer.println();
        }
        writer.close();
        
        BufferedReader r = new BufferedReader(new FileReader("output.txt"));
        int lines = 0;
        String line[] = new String[100000];
        while ((line[lines] = r.readLine()) != null) 
        {
            lines++;
        }
        r.close();
        
        Pair boxes[] = new Pair[1000];
        boolean found;
        boxes[0] = new Pair(line[0]);
        int count = 1;
        
        for (int i = 1; i < lines; i++) 
        {
            found = false;
            for (int j = 0; j < count; j++) 
            {
                
                if (boxes[j].getEncrypted().equals(line[i])) 
                {
                    boxes[j].increment();
                    found = true;
                    break;
                }
            }
            
            if (!found) 
            {
                boxes[count] = new Pair(line[i]);
                count++;
            }
        }
        
       
        for (int i = 0; i < count; i++) 
        {
            for (int j = i + 1; j < count; j++) 
            {
                if (boxes[i].getFreq() < boxes[j].getFreq()) 
                {
                    Pair x = boxes[i];
                    boxes[i] = boxes[j];
                    boxes[j] = x;
                }
            }
        }

        
        FileReader f = new FileReader("custom.txt");
        char[] letter = new char[70];
        int boxesCount = 0;
        int k;
        while ((k = f.read()) != -1) 
        {
            if ((char) k != '\n') {
                letter[boxesCount] = (char) k;
                boxesCount++;
            }
        }
        f.close();

        for (int i = 0; i < count; i++) 
        {
            boxes[i].setLetter(letter[i]);
        }

       
        System.out.println("And after 'cracking' faulty RSA:");
        for (int i = 0; i < lines; i++) 
        {
            for (int j = 0; j < count; j++) 
            {
                if (line[i].equals(boxes[j].getEncrypted())) 
                {
                    System.out.print(boxes[j].getLetter());
                }
            }
        }
        System.out.println();
    }

}
