#include <iostream>
#include <algorithm>
#include <cstring>

class String
{
private:
    char* buffer = nullptr;
    int length;
    
public:
        String(const char* str)
        {
            length = std::strlen(str);
            buffer = new char[length+1];
            if(buffer)
                std::copy(str,str+length, buffer);
            else
            {
                std::cerr << "Memóriafoglalási hiba\n";
                exit(-666);
            }
        }
        String(const String& otherString)
        {
              *this = otherString;
        }
        String& operator=(const String& otherString)
        {
            
            if(this != &otherString)
            {
               
                
                delete[] buffer;
                length = otherString.length;
                buffer = new char[length+1];
                std::cout << "hmm\n";
                if(buffer)
                {
                    std::copy(otherString.buffer,otherString.buffer+length, buffer);
                }
                else
                {
                    std::cerr << "MemAlloc error on buffer\n";
                    exit(-666);
                }
            
            }
           
            return *this;
        }
        String(String&& otherString)
        {
            buffer = nullptr;
            *this = std::move(otherString);
            
        }
        String& operator=(String&& otherString)
        {
            std::swap(buffer,otherString.buffer);
            length = otherString.length;
            return *this;
        }
        friend std::ostream& operator<< (std::ostream& os,const String& str)
        {
            os << str.buffer;
            return os;
        }
        char& operator[](int index) const
        {
            if(index >= length)
            {
                std::cerr << "NonoZone: index out of bounds\n";
                exit(-333);
            }
            return (buffer[index]);
        }
        char* getBuffer() const
        {
            return buffer;
        }
        int getLength() const
        {
            return length;
        }
        ~String()
        {
                delete[] buffer;
        }
};

void log(String msg)
{
    std::cout << msg << '\n';
}

int main()
{
    
    const String dude("It just works");
    std::cout << dude[1];
    
    log("Called constructor");
    log(dude);
    
    String dude2 = "It really just works";
    log("Called constructor");
    log(dude2);
    
    String dude3 = std::move(dude);
    log("Called move assignment");
    log(dude3);
    
    String dude4(std::move(dude3));
    log("Called move constructor");
    log(dude4);
    
    std::cout << "Extracting a thing from thing\n";
    std::cout << dude4[4] << " \n";
    
    std::cout << "Putting back another thing into the thing\n";
    dude4[4] = 'A';
    std::cout << dude4[4] << " \n";
   
    
    return 0;
    
}


