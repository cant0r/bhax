#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

#define SLEEPY_TIME 60000

typedef struct ball
{
    int x, y, stepX, stepY;
    char ballBody;
} Ball;

void initializeWindow(char,char,int);
void destroyWindow();

Ball* createBall(char, int, int);
void update(Ball* theBall);


int winHeight, winWidth;

int main()
{
    
    initializeWindow(0,0,0);
    getmaxyx(stdscr,winWidth,winHeight);
    
    char key;
    
    Ball* theBall = createBall('O',10,12);
    
    while((key = getch()) == ERR)
    {
       
        update(theBall);
        usleep(SLEEPY_TIME);
        refresh();
    }
    
    
    destroyWindow();
    
    
    free(theBall);
    return 0;
}

void initializeWindow(char side_sym, char tops_sym, int cur_mod)
{
    initscr();
    cbreak();
    noecho();
    
    nodelay(stdscr, TRUE);
    curs_set(cur_mod);
  
    box(stdscr, side_sym,tops_sym);
}
void destroyWindow()
{
    getch();
    endwin();
}
Ball* createBall(char body, int posX, int posY)
{
    if((posX < 0 || posX > winHeight) || (posY < 0 || posY > winWidth))
    {
        mvprintw(0,0, "NEGATÍV vagy ABLAKON KÍVÜLI pozíciók lettek megadva!");
        destroyWindow();
        exit(-1);
    }
    
    Ball* theBall = (Ball*)calloc(1,sizeof(Ball));    
    theBall->ballBody = body;
    theBall->x = (posX >= 2 && posX < winWidth - 3) ? posX : 2;
    theBall->y = (posY >= 2 && posY < winHeight - 3) ? posY : 2;
    theBall->stepX = 1;
    theBall->stepY = 1;
    
    return theBall;
}
void update(Ball* theBall)
{
    if((theBall->x < 2) || (theBall->x > winWidth-3))
    {    theBall->stepX*=-1;}
    if((theBall->y < 2) || (theBall->y > winHeight-3))
    {    theBall->stepY*=-1;}
    
    mvaddch(theBall->x, theBall->y, ' ' | A_NORMAL);
    
    theBall->x += theBall->stepX;
    theBall->y += theBall->stepY;
    
    mvaddch(theBall->x, theBall->y, theBall->ballBody );
}
