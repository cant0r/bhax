#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

#define SLEEPY_TIME 60000

typedef struct ball
{
    int x, y;
    char ballBody;
} Ball;

void initializeWindow(char,char,int);
void destroyWindow();

void update(Ball* theBall);


int winHeight, winWidth;
int helperX=0, helperY=0;

int main()
{
    
    initializeWindow(0,0,0);
    getmaxyx(stdscr,winWidth,winHeight);
    
    winWidth*=2;
    winHeight*=2;
    
    
    Ball theBall = {0, 0, 'o'};
    
    for(;;)
    {
       
        update(&theBall);
        usleep(SLEEPY_TIME);
        refresh();
    }
    
    
    destroyWindow();
    
    
    return 0;
}

void initializeWindow(char side_sym, char tops_sym, int cur_mod)
{
    initscr();
    cbreak();
    noecho();
    
    nodelay(stdscr, TRUE);
    curs_set(cur_mod);
  
    box(stdscr, side_sym,tops_sym);
}
void destroyWindow()
{
    getch();
    endwin();
}

void update(Ball* theBall)
{
    mvaddch(abs(theBall->y + (winWidth-helperY))/2,abs(theBall->x + (winHeight-helperX))/2 , ' ');
    theBall->x = (theBall->x-1) % winHeight;
    helperX =  (helperX+1) % winHeight;
    
    theBall->y = (theBall->y-1) % winWidth;
    helperY = (helperY+1) % winWidth;
    
    mvaddch(abs(theBall->y + (winWidth-helperY))/2,abs(theBall->x + (winHeight-helperX))/2 , theBall->ballBody);
    
    box(stdscr, 0,0); // this is a hack, very simple hack, heck
}
