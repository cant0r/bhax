library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    indexes = which(diff==2)
    primes_1 = primes[indexes]
    primes_2 = primes[indexes]+2
    sumOfPrimes = 1/primes_1+1/primes_2
    return(sum(sumOfPrimes))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
