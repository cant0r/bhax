
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Random;

public class Breakout {
    
    public class Barrier
    {
        private int x, y;
        private int width, height;
        private Color color;
        public boolean display;
        public Barrier(int x, int y, Color color)
        {
            this.x = x;
            this.y = y;
            this.color = color;
            width = 96;
            height = 45;
            display = true;
        }
        
        public boolean collidesWith(int ballX, int ballY)
        {
            if((((ballX >= this.x) && (ballX <= this.x+width)) && 
                    ((ballY >= this.y) && (ballY <= this.y+height)))
                    ||
                    (((ballX+radius >= this.x) && (ballX+radius <= this.x+width)) && 
                    ((ballY+radius >= this.y) && (ballY+radius <= this.y+height)))
                    ||
                    (((ballX >= this.x) && (ballX <= this.x+width)) && 
                    ((ballY+radius >= this.y) && (ballY+radius <= this.y+height)))
                    ||
                    (((ballX+radius >= this.x) && (ballX+radius <= this.x+width)) && 
                    ((ballY >= this.y) && (ballY <= this.y+height))))
                return true;
            return false;
        }
        
    }
    
    int  x = 800;
    int  y = 600;
    int radius = 50;
    int stepX=1, stepY=1;
    int maxX, maxY;
    private ArrayList<Barrier> barriers = new ArrayList<>();
    private static DisplayMode[] BEST_DISPLAY_MODES = new DisplayMode[]{
        new DisplayMode(1920, 1080, 32, 0),
        new DisplayMode(1920, 1080, 16, 0),
        new DisplayMode(1920, 1080, 8, 0)
    };
    Frame mainFrame;
    Random rgbRandomizer = new Random();
    Color backgroundColor, ballColor;
    public void newBarriers(ArrayList<Barrier> rects)
    {
        
        for(int i=0; i<6; i++)
            for(int j=0; j<20; j++)
            {
                barriers.add(new Barrier(j*96,i*45, new Color(rgbRandomizer.nextInt(256),
                    rgbRandomizer.nextInt(256),
                rgbRandomizer.nextInt(256))));
            }
    }
    public void move() {
       if(x <= 50 || x >=maxX-50)
       {
            stepX*=-1;
          
       }     
       if(y <=50 || y >= maxY-50)
       {
            stepY*=-1;
            
       }    
       for(Barrier b : barriers)
           {
               if(b.display && b.collidesWith(x,y))
               {
                   b.display = false;
                  
                   if(x > b.x)
                   {
                       stepX*=-1;
                      
                   }
                   else
                   {
                        stepY*=-1;
                      
                   }
                   
                  
               }
           }
       x+=stepX;
       y+=stepY;
    }

    public Breakout(int numBuffers, GraphicsDevice device) {
        try {
          
            GraphicsConfiguration gc = device.getDefaultConfiguration();
            mainFrame = new Frame(gc);
            mainFrame.setUndecorated(true);
            mainFrame.setIgnoreRepaint(true);
            device.setFullScreenWindow(mainFrame);
            if (device.isDisplayChangeSupported()) {
                chooseBestDisplayMode(device);
            }
            Rectangle bounds = mainFrame.getBounds();
            bounds.setSize(device.getDisplayMode().getWidth(), device.getDisplayMode().getHeight());
            maxX = device.getDisplayMode().getWidth();
            maxY = device.getDisplayMode().getHeight();
            mainFrame.createBufferStrategy(numBuffers);
            BufferStrategy bufferStrategy = mainFrame.getBufferStrategy();
            newBarriers(barriers);
            backgroundColor = new Color(14,141,145);
            ballColor = new Color(214,214,28);
            while (true) {
                Graphics g = bufferStrategy.getDrawGraphics();
                if (!bufferStrategy.contentsLost()) {
                    move();                    
                    g.setColor(backgroundColor);                
                    g.fillRect(0, 0, bounds.width, bounds.height);
                    for(Barrier b : barriers)
                    {
                            if(b.display == false)
                            {
                                continue;
                            }
                            g.setColor(b.color);
                            g.fillRect(b.x,b.y,b.width, b.height);
                                                                         
                    }
                    g.setColor(ballColor);
                    g.fillOval(x, y, radius, radius);
                    //g.drawLine(startX+radius/2, startY+radius/2, x+radius/2, y+radius/2);
                    bufferStrategy.show();
                    g.dispose();
                    
                }
                try {
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            device.setFullScreenWindow(null);
        }

    }

    private static DisplayMode getBestDisplayMode(GraphicsDevice device) {
        for (int x = 0; x < BEST_DISPLAY_MODES.length; x++) {
            DisplayMode[] modes = device.getDisplayModes();
            for (int i = 0; i < modes.length; i++) {
                if (modes[i].getWidth() == BEST_DISPLAY_MODES[x].getWidth()
                        && modes[i].getHeight() == BEST_DISPLAY_MODES[x].getHeight()
                        && modes[i].getBitDepth() == BEST_DISPLAY_MODES[x].getBitDepth()) {
                    return BEST_DISPLAY_MODES[x];
                }
            }
        }
        return null;
    }

    public static void chooseBestDisplayMode(GraphicsDevice device) {
        DisplayMode best = getBestDisplayMode(device);
        if (best != null) {
            device.setDisplayMode(best);
        }
    }

    public static void main(String[] args) {
        try {
            int numBuffers = 2;

            GraphicsEnvironment env = GraphicsEnvironment.
                    getLocalGraphicsEnvironment();
            GraphicsDevice device = env.getDefaultScreenDevice();
            Breakout ball = new Breakout(numBuffers, device);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);

    }
}
