#!/bin/bash

exponents=(6 7 8)

echo "A BBP algoritmus mérése a következő gépen:"
neofetch

echo "JAVA program mérési eredményei"

for exponent in ${exponents[*]}
do
	time java BPP $((exponent))
	echo
done

echo "C program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./BPP $((exponent))
	echo
done

echo "C++ program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./BPP_PLUS $((exponent))
	echo
done

echo "C# program mérési eredményei"

for exponent in ${exponents[*]}
do
    cd CSHARP
	time dotnet run $((exponent))
	echo
	cd ..
done


echo "Teszt sikeresen végrehajtva!"
