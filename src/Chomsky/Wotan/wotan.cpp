#include <cstdio>
#include <signal.h>


int f(int a)
{
	return a*2;
}
int f(int* a)
{
	return *a;
}
int f(int a, int b)
{
	printf("HELLÓ!: %d %d\n", a,b);
	return a;
}

void jelkezelo(int jel)
{
	printf("WOW\n");
}

int main()
{
	int i=0; 
	int a =1;
	for(i=0; i<5; ++i)
		printf("%d ",i);

	putchar('\n'); 
	i=0;
	for(i=0; i<5; i++)
		printf("%d ", i);

	putchar('\n');
	putchar('\n');

	int tomb[] = {5,4,3, 2, 1};
	i = 0;
	for(i=0; i<5; printf("%d\n",tomb[i] = i++));

	int da[4] = {1,2,3,4};
	int* d = da;
	int sa[4] = {5,6,7,8};
	int* s = sa;
	int n=4;
	
	for(i=0; i<n && (*d++ = *s++); ++i);
	i = 0;
	for( i=0; i<4; i++)
		printf("%d ", d[i]);

	putchar('\n');


	a=0;
	printf("%d %d", f(&a), a);
	putchar('\n');
	printf("%d %d", f(a), a);
	putchar('\n');
	printf("\t%d %d", f(a, ++a), f(++a, a));
	putchar('\n');
	if(signal(SIGINT, SIG_IGN) != SIG_IGN)
		signal(SIGINT, jelkezelo);

	return 0;
}
