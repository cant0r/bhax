import java.io.FileNotFoundException;
import java.io.PrintWriter;

public aspect aop
{
	int depth = 0;
    /*
     * public pointcut callFun(char b) : call(public void LZWBinaryTree.insert(char)) && args(b);
    before(char b) : callFun(b)
    {
        System.out.println("Adding " + b + " into the Treee");
    }
    after(char b) : callFun(b)
    {
        System.out.println("We have successfully included " + b + " into the Treee");
    }*/
	public pointcut travel(LZWBinaryTree.Node n, PrintWriter os) 
	: call(public void LZWBinaryTree.printTree(LZWBinaryTree.Node, PrintWriter)) && args(n,os);
	
    after(LZWBinaryTree.Node n, PrintWriter os) throws FileNotFoundException : travel(n, os)
    {
    	inOrder(n,new PrintWriter("in-order.txt"));
    	depth = 0;
    	postOrder(n,new PrintWriter("post-order.txt"));
    }
    
    
    public void inOrder(LZWBinaryTree.Node n, PrintWriter p)
    {
    	if (n != null)
        {
            ++depth;
            for (int i = 0; i < depth; ++i)
                p.print("---");
            p.print(n.getValue () + "(" + depth + ")\n");
            inOrder (n.getLeftChild (), p);
            inOrder (n.getRightChild (), p);
            --depth;
        }
    }
    public void postOrder(LZWBinaryTree.Node n, PrintWriter p)
    {
    	if (n != null)
        {
            ++depth;
            postOrder (n.getLeftChild (), p);
            postOrder (n.getRightChild (), p);
            for (int i = 0; i < depth; ++i)
                p.print("---");
            p.print(n.getValue () + "(" + depth + ")\n");
            --depth;
        }
    }
    
    
}
