package com.cantor.simon;

import androidx.appcompat.app.AppCompatActivity;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
{

    Simon theJester;
    ArrayList<Character> userResponse;
    ArrayList<Button> colorButtons;

    Button startBtn;
    int clicks;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startBtn = (Button) findViewById(R.id.btnStart);
        colorButtons = new ArrayList<>();
        ViewGroup btnContainer = (ViewGroup)  findViewById(R.id.root);
        int n = btnContainer.getChildCount();
        for(int i=0; i<4 ; i++)
        {
            colorButtons.add((Button) btnContainer.getChildAt(i));
        }
        for(Button b : colorButtons)
        {
            b.setEnabled(false);
        }
        ((Button)findViewById(R.id.red_btn)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                userResponse.add('r');
                clicks--;
                if(clicks == 0)
                    checkSequence();
            }
        });
        ((Button)findViewById(R.id.blue_btn)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                userResponse.add('b');
                clicks--;
                if(clicks == 0)
                    checkSequence();
            }
        });
        ((Button)findViewById(R.id.yellow_btn)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                userResponse.add('y');
                clicks--;
                if(clicks == 0)
                    checkSequence();
            }
        });
        ((Button)findViewById(R.id.green_btn)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                userResponse.add('g');
                clicks--;
                if(clicks == 0)
                    checkSequence();
            }
        });
    }

    private void checkSequence()
    {
        if(theJester.checkEquality(userResponse) == true)
        {
            Toast.makeText(this, "You win! Increasing difficulty!", Toast.LENGTH_SHORT).show();
            ((TextView)findViewById(R.id.diffLbl)).setText(String.valueOf(theJester.getN()));

            theJester = new Simon(theJester.getN()+1);
            userResponse = new ArrayList<>();
            clicks = theJester.getN();
            startBtn.setVisibility(View.VISIBLE);
            for(Button b : colorButtons)
            {
                b.setEnabled(false);
            }
        }
        else
        {
            Toast.makeText(this,"You lose! BYE-BYE", Toast.LENGTH_LONG).show();
            finishAndRemoveTask();
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    System.exit(0);
                }
            }, 1000);

        }


    }

    private void playSequence()
    {
        final ArrayList<Character> colorSeq = theJester.getSequence();
        final ArrayList<Animator> anims = new ArrayList<>();
        final AnimatorSet set = new AnimatorSet();
        //Toast.makeText(this,colorSeq.toString(),Toast.LENGTH_SHORT).show();
        for(int i=0; i < colorSeq.size(); i++)
        {

            switch(colorSeq.get(i).charValue())
            {
                case 'r':
                    Animator a = ObjectAnimator.ofFloat(colorButtons.get(0), View.ALPHA, 1, 0).setDuration(350);
                    Animator b = ObjectAnimator.ofFloat(colorButtons.get(0), View.ALPHA, 0, 1).setDuration(350);
                    anims.add(a);
                    anims.add(b);
                    break;
                case 'g':
                    Animator aa = ObjectAnimator.ofFloat(colorButtons.get(3), View.ALPHA, 1, 0).setDuration(350);
                    Animator bb = ObjectAnimator.ofFloat(colorButtons.get(3), View.ALPHA, 0, 1).setDuration(350);
                    anims.add(aa);
                    anims.add(bb);
                    break;
                case 'b':
                    Animator aaa = ObjectAnimator.ofFloat(colorButtons.get(1), View.ALPHA, 1, 0).setDuration(350);
                    Animator bbb = ObjectAnimator.ofFloat(colorButtons.get(1), View.ALPHA, 0, 1).setDuration(350);
                    anims.add(aaa);
                    anims.add(bbb);
                    break;
                case 'y':
                    Animator aaaa = ObjectAnimator.ofFloat(colorButtons.get(2), View.ALPHA, 1, 0).setDuration(350);
                    Animator bbbb = ObjectAnimator.ofFloat(colorButtons.get(2), View.ALPHA, 0, 1).setDuration(350);
                    anims.add(aaaa);
                    anims.add(bbbb);
                    break;

            }
        }
       // Toast.makeText(this, String.valueOf(anims.size()), Toast.LENGTH_SHORT).show();
        set.playSequentially(anims);
        set.start();
    }


    @Override
    protected void onStart()
    {
        super.onStart();
        theJester = new Simon(4);
        userResponse = new ArrayList<>();
        clicks = theJester.getN();


        startBtn.setOnClickListener(null);
        startBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                for(Button b : colorButtons)
                {
                    b.setEnabled(true);
                }
                v.setVisibility(View.INVISIBLE);
                playSequence();
            }
        });

    }
    @Override
    protected void onPause()
    {
        super.onPause();
        finishAndRemoveTask();
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                System.exit(0);
            }
        }, 1000);
    }


}
