package com.cantor.simon;

import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Simon
{
    private ArrayList<Character> seq;
    private char[] colors;
    private int difficulty;

    public Simon(int diff)
    {
        colors = new char[]{'r', 'g', 'b', 'y'};
        seq = new ArrayList<>();
        difficulty = diff;
        generateSeq();
    }

    private void generateSeq()
    {
        Random generator = new Random();
        for(int i=0; i<difficulty; i++)
        {
            seq.add(colors[generator.nextInt(difficulty) % colors.length]);
        }

    }

    public ArrayList<Character> getSequence()
    {
        return seq;
    }

    public boolean checkEquality(ArrayList<Character> response)
    {
        return seq.equals(response);
    }
    public int getN()
    {
        return difficulty;
    }
}
