import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author cantor
 */
public class LZWUnitTest {
    
    LZWBinaryTree testTree = new LZWBinaryTree();
    String input = "01111001001001000111";
    
    @BeforeAll
    public static void startTesting() 
    {
        System.out.println("I'm about to test this tree, my dude.");
       
    }
    
    @AfterAll
    public static void endTesting() 
    {
        System.out.println("It's all cool, my dude.");
    }
    
    @Test
    public void testAverage()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        
        assertEquals(2.75, testTree.getMean(), 0.0001);
        System.out.println("A várt átlag megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 2.75,testTree.getMean());
    }
    @Test
    public void testDepth()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(4 , testTree.getDepth());
        System.out.println("A várt mélység megegyezik a kapottal!");
        System.out.printf("Várt: %d Kapott: %d\n", 4,testTree.getDepth());
    }
    @Test
    public void testVariance()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(0.9574, testTree.getVariance(), 0.0001);
        System.out.println("A várt szórás megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 0.9574,testTree.getVariance());
    }
}
