// mandelpngc_60x60_100.cu
// Copyright (C) 2019
// Norbert Bátfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Version history
//
//  Mandelbrot png
//  Programozó Páternoszter/PARP
//   https://www.tankonyvtar.hu/hu/tartalom/tamop412A/2011-0063_01_parhuzamos_prog_linux
//
//  https://youtu.be/gvaqijHlRUs
//
// Modified by Racs Tamás on 2019.03.26

#include <png++/image.hpp>
#include <png++/rgb_pixel.hpp>

#include <sys/times.h>
#include <iostream>


#define SIZE 600
#define ITERATION_LIMIT 32000

__device__ int
mandel (int k, int j)
{
  // Végigzongorázza a CUDA a szélesség x magasság rácsot:
  // most eppen a j. sor k. oszlopaban vagyunk

  // számítás adatai
  float a = -2.0, b = .7, c = -1.35, d = 1.35;
  int width = SIZE, height = SIZE, iterationLimit = ITERATION_LIMIT;

  // a számítás
  float dx = (b - a) / width;
  float dy = (d - c) / height;
  float reC, imC, reZ, imZ, ujreZ, ujimZ;
  // Hány iterációt csináltunk?
  int iteration = 0;

  // c = (reC, imC) a rács csomópontjainak
  // megfelelő komplex szám
  reC = a + k * dx;
  imC = d - j * dy;
  // z_0 = 0 = (reZ, imZ)
  reZ = 0.0;
  imZ = 0.0;
  iteration = 0;
  // z_{n+1} = z_n * z_n + c iterációk
  // számítása, amíg |z_n| < 2 vagy még
  // nem értük el a 255 iterációt, ha
  // viszont elértük, akkor úgy vesszük,
  // hogy a kiinduláci c komplex számra
  // az iteráció konvergens, azaz a c a
  // Mandelbrot halmaz eleme
  while (reZ * reZ + imZ * imZ < 4 && iteration < iterationLimit)
    {
      // z_{n+1} = z_n * z_n + c
      ujreZ = reZ * reZ - imZ * imZ + reC;
      ujimZ = 2 * reZ * imZ + imC;
      reZ = ujreZ;
      imZ = ujimZ;

      ++iteration;

    }
  return iteration;
}


/*
__global__ void
mandelkernel (int *buffer)
{

  int j = blockIdx.x;
  int k = blockIdx.y;

  buffer[j + k * SIZE] = mandel (j, k);

}
*/

__global__ void
mandelkernel (int *buffer)
{

  int tj = threadIdx.x;
  int tk = threadIdx.y;

  int j = blockIdx.x * 10 + tj;
  int k = blockIdx.y * 10 + tk;

  buffer[j + k * SIZE] = mandel (j, k);

}

void
cudamandel (int buffer[SIZE][SIZE])
{

  int *deviceImageBuffer;
  cudaMalloc ((void **) &deviceImageBuffer, SIZE * SIZE * sizeof (int));

  // dim3 grid (SIZE, SIZE);
  // mandelkernel <<< grid, 1 >>> (deviceImageBuffer);
  
  dim3 grid (SIZE / 10, SIZE / 10);
  dim3 tgrid (10, 10);
  mandelkernel <<< grid, tgrid >>> (deviceImageBuffer);  
  
  cudaMemcpy (buffer, deviceImageBuffer,
	      SIZE * SIZE * sizeof (int), cudaMemcpyDeviceToHost);
  cudaFree (deviceImageBuffer);

}

int
main (int argc, char *argv[])
{

  // Mérünk időt (PP 64)
  clock_t delta = clock ();
  // Mérünk időt (PP 66)
  struct tms tmsbuf1, tmsbuf2;
  times (&tmsbuf1);

  if (argc != 2)
    {
      std::cout << "Hasznalat: ./mandelpngc fajlnev";
      return -1;
    }

  int buffer[SIZE][SIZE];

  cudamandel (buffer);

  png::image < png::rgb_pixel > image (SIZE, SIZE);

  for (int j = 0; j < SIZE; ++j)
    {
      //sor = j;
      for (int k = 0; k < SIZE; ++k)
	{
	  image.set_pixel (k, j,
			 png::rgb_pixel (255 -
					 (255 * buffer[j][k]) / ITERATION_LIMIT,
					 255 -
					 (255 * buffer[j][k]) / ITERATION_LIMIT,
					 255 -
					 (255 * buffer[j][k]) / ITERATION_LIMIT));
	}
    }
  image.write (argv[1]);

  std::cout << argv[1] << " mentve" << std::endl;

  times (&tmsbuf2);
  std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
    + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

  delta = clock () - delta;
  std::cout << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;

}
