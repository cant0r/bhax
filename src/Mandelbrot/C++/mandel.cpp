// mandelpngt.c++
// Copyright (C) 2019
// Norbert Bátfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Version history
//
//  Mandelbrot png
//  Programozó Páternoszter/PARP
//   https://www.tankonyvtar.hu/hu/tartalom/tamop412A/2011-0063_01_parhuzamos_prog_linux
//
//  https://youtu.be/gvaqijHlRUs
//
// Ported to the C language from C++ 2019.03.24 by Racs Tamás
// Ported from the ported C source to C++ 2019.03.26 by Racs Tamás
//
#include <iostream>
#include <png++/png.hpp>
#include <sys/times.h>
#include <complex>

#define SIZE 600
#define ITERATION_LIMIT 32000

void mandel (int buffer[SIZE][SIZE]) {

    clock_t delta = clock ();
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

    float a = -2.0, b = .7, c = -1.35, d = 1.35;
    int width = SIZE, height = SIZE, iterationLimit = ITERATION_LIMIT;

    // a számítás
    float dx = (b - a) / width;
    float dy = (d - c) / height;
    float reC, imC;
    
    int iteration = 0;
    
    
    
    for (int j = 0; j < height; ++j)
    {
        //sor = j;
        for (int k = 0; k < width; ++k)
        {
            
            reC = a + k * dx;
            imC = d - j * dy;
            std::complex<double> complexNum(reC,imC);
            std::complex<double> complexHelper(0,0);
          
            iteration = 0;
            
            while (std::abs(complexHelper) < 4 && iteration < iterationLimit)
            {
                
                complexHelper = complexHelper*complexHelper + complexNum;
                
                ++iteration;

            }

            buffer[j][k] = iteration;
        }
    }

    times (&tmsbuf2);
    std::cout <<tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime << "\n";

    delta = clock () - delta;
    std::cout << (float) delta / CLOCKS_PER_SEC << " sec\n";

}

int main (int argc, char *argv[])
{

    if (argc != 2)
    {
        std::cout << "Hasznalat: ./mandelpng fajlnev\n";
        return -1;
    }
    
    png::image < png::rgb_pixel > image(SIZE,SIZE);
    
    int buffer[SIZE][SIZE];

    mandel(buffer);

    for (int j = 0; j < SIZE; ++j)
    {
        //sor = j;
        for (int k = 0; k < SIZE; ++k)
        {
            image.set_pixel(k,j,png::rgb_pixel((255 - (255 * buffer[j][k]) / ITERATION_LIMIT)
                          ,(255 - (255 * buffer[j][k]) / ITERATION_LIMIT),
                           (255 - (255 * buffer[j][k]) / ITERATION_LIMIT)));
        }

    }
    image.write(argv[1]);

    std::cout << argv[1] <<" mentve\n";

}
